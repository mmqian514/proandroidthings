package edu.uchicago.maartenx01.blinktest;


import android.app.Activity;
import android.os.Bundle;

import java.io.IOException;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.things.contrib.driver.button.Button;
import com.google.android.things.contrib.driver.button.ButtonInputDriver;
import com.google.android.things.contrib.driver.pwmspeaker.Speaker;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Gpio mLed1Gpio;
    private Gpio mLed2Gpio;
    private ButtonInputDriver mButtonInputDriver;
    private Boolean buttonState = false;
    private android.widget.Button mToggleButton;
    private android.widget.Button mConvertButton;
    private android.widget.Button mSoundButton;
    private android.widget.Button mBoth;
    private EditText mString;
    private TextView mOut;
    private String[] mSymbols;
    private String[] mCode;
    private Handler mHandlerL = new Handler();
    private Handler mHandlerS = new Handler();
    private Handler mHandlerB = new Handler();
    private String morseCode = "";
    private int[] delays;
    private int[] speakerDelays;
    private String[] notes;
    private int SMALL = 200;
    private int BIG = 500;
    private int sSMALL = 100;
    private int sBig = 200;
    private int count  = 0;
    private int index = 0;
    public static final double G4 = 391.995;
    private Speaker mSpeaker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blink);
        mToggleButton = (android.widget.Button) findViewById(R.id.button2);
        mConvertButton = (android.widget.Button) findViewById(R.id.buttonL);
        mSoundButton = (android.widget.Button) findViewById(R.id.buttonS);
        mBoth = (android.widget.Button) findViewById(R.id.buttonboth);
        mString = (EditText) findViewById(R.id.editText);
        mOut = (TextView) findViewById(R.id.textView);
        mSymbols = getResources().getStringArray(R.array.letters);
        mCode = getResources().getStringArray(R.array.code);


        Log.i(TAG, "Starting ButtonActivity");
        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleLed();
            }
        });

        mConvertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConvertString();
            }
        });

        mSoundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConvertSound();
            }
        });

        mBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConvertBoth();
            }
        });


        PeripheralManager pioService = PeripheralManager.getInstance();
        try {
            Log.i(TAG, "Configuring GPIO pins");
            mLed1Gpio = pioService.openGpio(BoardDefaults.getGPIOForLED1());
            mLed1Gpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);

            Log.i(TAG, "Registering button driver");
            // Initialize and register the InputDriver that will emit SPACE key events
            // on GPIO state changes.
            mButtonInputDriver = new ButtonInputDriver(
                    BoardDefaults.getGPIOForButton(),
                    Button.LogicState.PRESSED_WHEN_LOW,
                    KeyEvent.KEYCODE_SPACE);
        } catch (IOException e) {
            Log.e(TAG, "Error configuring GPIO pins", e);
        }
        try {
            Log.i(TAG, "Configuring GPIO pins");
            mLed2Gpio = pioService.openGpio(BoardDefaults.getGPIOForLED2());
            mLed2Gpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);

            Log.i(TAG, "Registering button driver");

        } catch (IOException e) {
            Log.e(TAG, "Error configuring GPIO pins", e);
        }
        try {
            mSpeaker = new Speaker(BoardDefaults.getPwmPin());
            mSpeaker.stop(); // in case the PWM pin was enabled already
        } catch (IOException e) {
            Log.e(TAG, "Error initializing speaker");
            return; // don't initilize the handler
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mButtonInputDriver.register();
    }

    private void getMorse(String input,Integer state){
        String output = "";
        boolean found = false;
        for (int j=0;j<input.length();j++){
            found = false;
            for (int i=0;i<mSymbols.length;i++){
                if (mSymbols[i].charAt(0) == Character.toLowerCase(input.charAt(j))){
                    for (int k=0;k<mCode[i].length();k++){
                        morseCode += mCode[i].charAt(k);
                        morseCode += mCode[i].charAt(k);
                        found = true;
                    }
                    output += mCode[i];
                }

            }
            output += "    ";
            if (found==false){
                Toast.makeText(this,"Enter valid string",Toast.LENGTH_SHORT);
                mOut.setText("Enter valid string");
                return;

            }
        }

        mOut.setText(output);
        delays = new int[morseCode.length()];
        speakerDelays = new int[morseCode.length()];
        for (int i=0;i<morseCode.length();i++){
            if (morseCode.charAt(i) == '-'){
                delays[i] = BIG;
                speakerDelays[i] = sBig;
            }
            else{
                delays[i] = SMALL;
                speakerDelays[i] = sSMALL;
            }
        }



        if (state==0){
            mHandlerL.post(mBlinkRunnable);
        }
        else if(state == 1){
            mHandlerS.post(mPlaybackRunnable);
        }
        else{
            mHandlerB.post(mDoBoth);
        }



    }

    private Runnable mBlinkRunnable = new Runnable() {
        @Override
        public void run() {
            // Exit if the GPIO is already closed
            if (mLed1Gpio == null) {
                return;
            }

            try {
                // Step 3. Toggle the LED state
                if (count ==delays.length) {
                    // reached the end
                    try{
                        mLed1Gpio.setValue(false);
                    }catch (IOException e){
                        Log.e(TAG, "Error on PeripheralIO API", e);
                    }
                    return;
                } else {
                    if (count % 2 == 0) {
                        try {
                            mLed1Gpio.setValue(true);
                        } catch (IOException e) {
                            Log.e(TAG, "Error on PeripheralIO API", e);
                        }

                    } else {
                        try {
                            mLed1Gpio.setValue(false);
                        } catch (IOException e) {
                            Log.e(TAG, "Error on PeripheralIO API", e);
                        }
                    }
                    // Step 4. Schedule another event after delay.
                    mHandlerL.postDelayed(mBlinkRunnable, delays[count]);
                }
                count = count+1;
            } catch (Exception e) {
                Log.e(TAG, "Error", e);
            }

        }
    };

    private void ConvertString(){
        String value = mString.getText().toString();

        if (!value.isEmpty()){

            try{
                mLed1Gpio.setValue(false);
            }catch (IOException e){
                Log.e(TAG, "Error on PeripheralIO API", e);
            }
            count = 0;
            morseCode = "";
            getMorse(value,0);
        }else{
            Toast.makeText(this,"Enter valid string",Toast.LENGTH_SHORT);
            mOut.setText("Enter valid string");

        }

    }

    private void ConvertBoth(){
        String value = mString.getText().toString();

        if (!value.isEmpty()){

            try{
                mLed1Gpio.setValue(false);
            }catch (IOException e){
                Log.e(TAG, "Error on PeripheralIO API", e);
            }
            index = 0;
            morseCode = "";
            getMorse(value,2);
        }else{
            Toast.makeText(this,"Enter valid string",Toast.LENGTH_SHORT);
            mOut.setText("Enter valid string");

        }

    }
    private Runnable mDoBoth = new Runnable() {

        @Override
        public void run() {
            if (mSpeaker == null) {
                return;
            }

            try {
                if (index ==speakerDelays.length) {
                    // reached the end
                    mSpeaker.stop();
                    try{
                        mLed1Gpio.setValue(false);
                    }catch (IOException e){
                        Log.e(TAG, "Error on PeripheralIO API", e);
                    }
                    return;
                } else {
                    double note = G4;
                    if (index%2==0){
                        mSpeaker.play(note);
                        try {
                            mLed1Gpio.setValue(true);
                        } catch (IOException e) {
                            Log.e(TAG, "Error on PeripheralIO API", e);
                        }
                    }
                    else{
                        mSpeaker.stop();
                        try {
                            mLed1Gpio.setValue(false);
                        } catch (IOException e) {
                            Log.e(TAG, "Error on PeripheralIO API", e);
                        }
                    }
                    mHandlerB.postDelayed(this, speakerDelays[index]);
                }
                index +=1;
            } catch (IOException e) {
                Log.e(TAG, "Error playing speaker", e);
            }
        }
    };

    private void ConvertSound(){
        String value = mString.getText().toString();
        if (!value.isEmpty()){
            morseCode = "";
            index = 0;
            getMorse(value,1);
        }
        else{
            Toast.makeText(this,"Enter valid string",Toast.LENGTH_SHORT);
            mOut.setText("Enter valid string");
        }

    }

    private void toggleLed(){

        if (buttonState == false){
            try{
                mLed2Gpio.setValue(true);
            }catch(IOException e){
                Log.e(TAG, "Error on PeripheralIO API", e);
            }

            buttonState = true;
        }
        else{
            try{
                mLed2Gpio.setValue(false);
            }catch(IOException e){
                Log.e(TAG, "Error on PeripheralIO API", e);
            }
            buttonState = false;
        }
    }


    private Runnable mPlaybackRunnable = new Runnable() {

        @Override
        public void run() {
            if (mSpeaker == null) {
                return;
            }

            try {
                if (index ==speakerDelays.length) {
                    // reached the end
                    mSpeaker.stop();
                    return;
                } else {
                    double note = G4;
                    if (index%2==0){
                        mSpeaker.play(note);
                    }
                    else{
                        mSpeaker.stop();
                    }
                    mHandlerS.postDelayed(this, speakerDelays[index]);
                }
                index +=1;
            } catch (IOException e) {
                Log.e(TAG, "Error playing speaker", e);
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_SPACE) {
            // Turn on the LED

            if (buttonState == false){
                try{
                    mLed2Gpio.setValue(true);
                }catch(IOException e){
                    Log.e(TAG, "Error on PeripheralIO API", e);
                }
                buttonState = true;
            }
            else{
                try{
                    mLed2Gpio.setValue(false);
                }catch(IOException e){
                    Log.e(TAG, "Error on PeripheralIO API", e);
                }
                buttonState = false;
            }


            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SPACE) {
            // Turn off the LED

            if (buttonState == false){
                try{
                    mLed2Gpio.setValue(true);
                }catch(IOException e){
                    Log.e(TAG, "Error on PeripheralIO API", e);
                }
                buttonState = true;

            }
            else{
                try{
                    mLed2Gpio.setValue(false);
                }catch(IOException e){
                    Log.e(TAG, "Error on PeripheralIO API", e);
                }
                buttonState = false;
            }

        }

        return super.onKeyUp(keyCode, event);
    }



    @Override
    protected void onDestroy(){
        super.onDestroy();
        mHandlerL.removeCallbacks(mBlinkRunnable);
        mHandlerS.removeCallbacks(mPlaybackRunnable);
        mHandlerB.removeCallbacks(mDoBoth);

        if (mButtonInputDriver != null) {
            mButtonInputDriver.unregister();
            try {
                mButtonInputDriver.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing Button driver", e);
            } finally{
                mButtonInputDriver = null;
            }
        }

        if (mLed1Gpio != null) {
            try {
                mLed1Gpio.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing LED GPIO", e);
            } finally{
                mLed1Gpio = null;
            }
            mLed1Gpio = null;
        }

        if (mLed2Gpio != null) {
            try {
                mLed2Gpio.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing LED GPIO", e);
            } finally{
                mLed2Gpio = null;
            }
            mLed2Gpio = null;
        }
    }
}